// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import Vuex from 'vuex'
import VueGtag from "vue-gtag";

Vue.use(VueGtag, {
  config: { id: "UA-24708255-8" }
}, router);
Vue.use(Vuex)
Vue.config.productionTip = false


const store = new Vuex.Store( {
	state: {
		title: ''
	},
	mutations: {
		rtChangeTitle( state, value ) {
			state.title = value;
			document.title = ( state.title ? state.title + ' - ' : '' ) + 'Dev Leader';
		}
	}
} );


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App: () => import('./layout/Main/Main') },
  template: '<App/>'
})
