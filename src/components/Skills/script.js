export default {
  name: 'Skills',
  data() {
    return{
      header: null,
      title: 'Skills',
      skills: [
        {name: 'JavaScript'},
        {name: 'Vue.JS'},
        {name: 'HTML'},
        {name: 'CSS'},
        {name: 'SCSS'},
        {name: 'UX'},
        {name: 'UI'},
        {name: 'Git'},
        {name: 'CI/CD'},
        {name: 'CMS Wordpress'},
        {name: 'Работа в команде'},
        {name: 'Управление командой'},
        {name: 'Расстановка приоритетов'},
        {name: 'Постановка задач разработчикам'},
        {name: 'Agile Project Management'},
        {name: 'Atlassian Jira'},
        {name: 'Atlassian Confluence'},
      ], 
    }
  },
}

