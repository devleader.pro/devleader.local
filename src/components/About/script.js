export default {
  name: 'Bio',
  data() {
    return{
      scrollY: null
    }
  },
  methods: {
    calculateScrollY(){
      this.scrollY = window.scrollY 
    }
  },
  mounted(){
    this.calculateScrollY
    window.addEventListener('scroll', this.calculateScrollY);
  }
}