export default {
  name: 'Work',
  data() {
    return{
      lang: 'ru',
      title: 'Опыт работы',
      subtitle: '',

      items: [{
        dateString: '2015.апрель - ...',
        dateStart: 1427846400000,
        dateEnd: new Date(),
        city: '',
        speciality: 'Frontend-разработчик',
        company: 'Фриланс',
        description: '<p>— Внедрение и исправление функционала на существующие сайты, удаление backdoor скриптов, исправление верстки;</p><p>— Реализация сайтов под требования заказчиков на CMS Wordpress и 1C:Bitrix;</p><p>— Соаздние и исправление шаблонов для CMS Wordpress;</p><p>— Участие в проектах различных масштабов;</p>',
        skills: [
          'JavaScript', 'VueJS', 'NodeJS', 'HTML', 'CSS', 'CMS Wordpress', 'Git', 'CI/CD'
        ], 
      },{
        dateString: '2018.март — 2020.май',
        dateStart: 1519862400000,
        dateEnd: 1590796799000,
        city: 'Москва, Санкт-Петербург',
        speciality: 'Team-Lead frontend разработки',
        company: 'Русофт',
        description: '<p>— Управление проектами от получения бизнес задач до полной реализации;</p><p>— Анализ, декомпозиция, расстановка приоритетов по полученным задачам;</p><p>— Участие в интеграции Git и CI/CD в процесс разработки в компании;</p><p>— Точечное общение с заказчиками для уточнения вопросов и корректировки поставленных задач для достижения результата;</p><p>— Обучение сотрудников фреймворкам и проектам;</p><p>— Организация команд для участия в хакатонах;</p>',
        skills: [
          'JavaScript', 'VueJS', 'HTML', 'CSS', 'Git','CI/CD','Jira','Confluence','Scrum'
        ], 
      },{
        dateString: '2017.ноябрь — 2020.май',
        dateStart: 1510185600000,
        dateEnd: 1590796799000,
        city: 'Москва, Санкт-Петербург',
        speciality: 'Frontend-разработчик',
        company: 'Русофт',
        description: '<p>— Реализация клиент-серверных WEB-приложений. ДБО для физических лиц, игры на котировках, криптокошельки;</p><p>— Создание внутренней корпоративной социальной сети - full-stack;</p><p>— Разработка рекламных WEB-сайтов компании;</p><p>— Внесение предложений для улучшения UI/UX в проектах;</p><p>— Исправление простых багов в iOS приложениях;</p>',
        skills: [
          'JavaScript', 'VueJS', 'NodeJS', 'HTML', 'CSS', 'PHP', 'Git', 'CI/CD'
        ], 
      },{
        dateString: '2016.август - 2017.август',
        dateStart: 1470009600000,
        dateEnd: 1504051200000,
        city: 'Санкт-Петербург',
        speciality: 'Frontend-разработчик',
        company: 'RedFox',
        description: '<p>— Рефакторинг каталога и смарт-фильтра для увеличения производительности в интернет-магазине на 1C:Битрикс;</p><p>— Реализация функционала для рекламных и маркетинговых кампаний, рекламных адаптивных материалов (баннеры, лонгриды, рассылки, брендирование), новых дизайнов для интернет-магазинов;</p><p>— Рефакторинг кода для уменьшения количества запросов на сервер при навигации по сайту, работы со скидками со стороны бэкенда;</p><p>— Общение с маркетологами и дизайнерами для увеличения конверсии интернет-магазинов;</p>',
        skills: [
          'JavaScript', 'HTML', 'CSS', 'PHP', 'UI/UX', 'Git'
        ], 
      },{
        dateString: '2012.август - 2015.март',
        dateStart: 1343779200000,
        dateEnd: 1427760000000,
        city: 'Санкт-Петербург',
        speciality: 'HTML-верстальщик',
        company: 'РосБизнесКонсалтинг',
        description: '<p>— Реализация нового дизайна на основной и партнерские сайты знакомств;</p><p>— Реализация рекламных кампаний (брендирование, баннеры, рекламные страницы, рекламные приложения);</p><p>— Реализация и доработка функционала на основном и партнерских сайтов знакомств;</p>',
        skills: [
          'HTML', 'CSS', 'Javascript', 'Photoshop', 'SVN', 
        ], 
      },{
        dateString: '2007.март - 2012.июль',
        dateStart: 1172707200000,
        dateEnd: 1343606400000,
        city: 'Пушкино',
        speciality: 'Дизайнер-верстальщик',
        company: 'Дикобраз-студия',
        description: '<p>— Корректировка вёрстки на существующих сайтах, создание шаблонов CMS</p><p>— Реализация дизайн макетов для полиграфии и web-интерфейсов, верстка html-страниц</p>',
        skills: [
          'HTML', 'CSS', 'PHP', 'Javascript', 'Photoshop', 'CMS Joomla', 'CMS Wordpress'
        ], 
      }]
    }
  },
  mounted(){
    this.expirens()
  }, 
  methods:{
    declOfNum(number, titles) {  
      let cases = [2, 0, 1, 1, 1, 2];  
      return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  

      for (let index = 0; index < array.length; index++) {
        const element = array[index];
        
      }
    },
    workExp(start, end ){
      let timestamp = (end ? end : new Date()) - start
      let month = Math.floor( (timestamp % 31556926000) / 2629743000 )
      let years = Math.floor(timestamp / 31556926000 )

      return `${years} ${this.declOfNum(years, ['год', 'года', 'лет'])} ${( month != 0 ) ? month : '' } ${( month != 0 ) ? this.declOfNum(month, ['месяц', 'месяца', 'месяцев']) : ''}`
    },
    expirens(){

      let timestamp = new Date() - 1174348800000
      let month = Math.floor( (timestamp % 31556926000) / 2629743000 )
      let years = Math.floor(timestamp / 31556926000 )

      this.subtitle = `Более ${years} ${this.declOfNum(years, ['года', 'лет', 'лет'])} ${( month != 0 ) ? month : '' } ${( month != 0 ) ? this.declOfNum(month, ['месяца', 'месяцев', 'месяцев']) : ''}`
      
    }
  }
}