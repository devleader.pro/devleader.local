export default {
  name: 'Education',
  data() {
    return{
      lang: 'ru',
      title: 'Образование',
      level: 'Среднее-специальное',
      items: [{
        year: '2013',
        speciality: 'Front-end разработчик (HTML, CSS, JavaScript, JQuery)',
        level: 'Повышение квалификации, курсы',
        establishment: 'Учебный Центр «Специалист» при МГТУ им. Н.Э.Баумана, Веб-технологии / Веб-программирование',
      },{
        year: '2006-2010',
        speciality: 'Реклама, Маркетинг',
        level:  'Среднее специальное',
        establishment: '1-й Московский Образовательный Комплекс (Технологический колледж №14)',
      }]
    }
  }
}