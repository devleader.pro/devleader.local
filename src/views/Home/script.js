export default {
  name: 'Home',
  components: {
    Bio: () => import('@/components/Bio/Bio.vue'),
    About: () => import('@/components/About/About.vue'),
    Skills: () => import('@/components/Skills/Skills.vue'),
    Work: () => import('@/components/Work/Work.vue'),
    Edu: () => import('@/components/Edu/Edu.vue'),
  },
}