export default {
  name: 'Main',
  components: {
    Header: ()=>import('@/layout/Header/Header'),
    Footer: ()=>import('@/layout/Footer/Footer'),
    Copyright: ()=>import('@/layout/Copyright/Copyright')
  },
}